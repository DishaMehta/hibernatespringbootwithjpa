package com.hibernateSpringBoot.Controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.hibernateSpringBoot.AppStart;
import com.hibernateSpringBoot.EntityClasses.Customer;
import com.hibernateSpringBoot.EntityClasses.CustomerLedger;
import com.hibernateSpringBoot.EntityClasses.Payment;
import com.hibernateSpringBoot.EntityClasses.Purchase;
import com.hibernateSpringBoot.Sevices.CreditcardService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value="Credit card rest controller",description = "Rest api related to credit card services")
@RestController
public class CreditcardController {
	
	static Logger logger = LogManager.getLogger(AppStart.class);
	@Autowired
	private CreditcardService creditCardService;
	
	@ApiOperation(value = "Add customer in the system")
	@RequestMapping(method=RequestMethod.POST,value="/customer")
	public void addCustomer(@RequestBody Customer customer) {
		logger.trace("Request for adding new customer");
		creditCardService.addCustomer(customer);
	}
	
	@ApiOperation(value = "Add purchase in the system")
	@RequestMapping(method=RequestMethod.POST,value="/purchase")
	public String addPurchase(@RequestBody Purchase purchase){
		logger.trace("Customer with id :: {} is making request for purchase with amount :: {}",purchase.getCustomerId(),purchase.getAmount());
		return creditCardService.addLedgerWithPurchase(purchase.getCustomerId(),purchase.getAmount());
	}
	
	@ApiOperation(value = "Add payment in the system")
	@RequestMapping(method=RequestMethod.POST,value="/payment")
	public String addPayment(@RequestBody Payment payment){
		logger.trace("Customer with ledger :: {} making request for payment with amount:: {}",payment.getLedgerId(),payment.getAmount());
		return creditCardService.AddLedgerPayment(payment.getLedgerId(),payment.getAmount());
	}
	
	@ApiOperation(value = "Fetch current balance of customer")
	@RequestMapping(method = RequestMethod.GET, value="/customers/{customerid}")
	public String fetchDetails(@PathVariable int customerid) {
		logger.trace("Customer making request for current balance with id :: {}",customerid);
		return creditCardService.fetchLatestBalance(customerid);
	}
	
	@ApiOperation(value = "Check open transaction of customer")
	@RequestMapping(method = RequestMethod.GET, value="/checkOpenTransation/{customerid}")
	public List<Integer> checkCustomerOpenBalance(@PathVariable int customerid) {
		logger.trace("Request for checking any open balance of customer with id :: {}",customerid);
		return creditCardService.checkOpenTransaction(customerid);
	}
}
 	