package com.hibernateSpringBoot.EntityClasses;

import java.math.BigDecimal;

public class Payment {
	int ledgerId;
	BigDecimal amount;
	public Payment() {
		
	}
	
	public Payment(int ledgerId, BigDecimal amount) {
		this.ledgerId = ledgerId;
		this.amount = amount;
	}

	public int getLedgerId() {
		return ledgerId;
	}
	public void setLedgerId(int ledgerId) {
		this.ledgerId = ledgerId;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	
	
	
}
