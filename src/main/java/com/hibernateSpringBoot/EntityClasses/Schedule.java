package com.hibernateSpringBoot.EntityClasses;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import io.swagger.annotations.ApiModelProperty;

@Entity
public class Schedule {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	
	@ApiModelProperty(notes = "Unique Id for schedule",name="schedule Id",required=false)
	private int scheduleId;
	
	@ApiModelProperty(notes = "Number of the schedule",name="schedule number",required=false)
	private int number;
	
	@ApiModelProperty(notes = "Schedule status",name="schedule status",required=false,allowableValues = "paid,unpaid")
	private String status;
	
	@ApiModelProperty(notes = "Amount for the schedule",name="schedule amount",required=false)
	private BigDecimal amount;
	
	@ManyToOne
	private CustomerLedger customerLedger;
	
	public Schedule() {

	}

	public Schedule(int number, String status, BigDecimal amount) {
		this.number = number;
		this.status = status;
		this.amount = amount;
	}

	public int getScheduleId() {
		return scheduleId;
	}

	public void setScheduleId(int scheduleId) {
		this.scheduleId = scheduleId;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public CustomerLedger getCustomerLedger() {
		return customerLedger;
	}

	public void setCustomerLedger(CustomerLedger customerLedger) {
		this.customerLedger = customerLedger;
	}

}
