package com.hibernateSpringBoot.EntityClasses;

import java.math.BigDecimal;

public class Purchase {
	int customerId;
	BigDecimal amount;
	
	public Purchase() {
	
	}
	
	public Purchase(int customerId, BigDecimal amount) {
		this.customerId = customerId;
		this.amount = amount;
	}

	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	
}
