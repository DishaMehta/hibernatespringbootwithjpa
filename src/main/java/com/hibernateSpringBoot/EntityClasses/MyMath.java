package com.hibernateSpringBoot.EntityClasses;

public class MyMath {
	int sum(int[] numbers) {
		int ans=0;
		for(int i=0;i<numbers.length;i++) {
			ans = ans + numbers[i];
		}
		return ans;
	}
}
