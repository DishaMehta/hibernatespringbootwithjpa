package com.hibernateSpringBoot.EntityClasses;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@Entity
public class CustomerLedger {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@ApiModelProperty(notes = "Unique id of the Customer Ledger",name="ledgerId",required=false)
	private int id;
	
	@ApiModelProperty(notes = "Type of the Customer Ledger",name="ledger type",required=false, allowableValues = "Purchase,Payment")
	private String type;
	
	@ApiModelProperty(notes = "Amount for the ledger",name="ledger amount",required=true)
	private BigDecimal amount;
	
	@ApiModelProperty(notes = "Balance for the ledger",name="ledger balance",required=true)
	private BigDecimal balance;
	
	@ApiModelProperty(notes = "Status for the ledger",name="ledger status",required=true,allowableValues = "Completed,Open")
	private String status; 
	@ManyToOne
	private Customer customer;
	
//	@OneToMany(mappedBy="customerLedger")
//	private Collection<Schedule>schedules = new ArrayList<Schedule>();

	
	public CustomerLedger() {
		
	}

	public CustomerLedger(Customer customer, String type, BigDecimal amount, BigDecimal balance, String status) {
		this.customer=customer;
		this.type = type;
		this.amount = amount;
		this.balance = balance;
		this.status = status;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

//	public Collection<Schedule> getSchedules() {
//		return schedules;
//	}
//
//	public void setSchedules(Collection<Schedule> schedules) {
//		this.schedules = schedules;
//	}

}
