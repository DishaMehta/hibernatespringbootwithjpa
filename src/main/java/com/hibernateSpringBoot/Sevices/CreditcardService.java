package com.hibernateSpringBoot.Sevices;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hibernateSpringBoot.AppStart;
import com.hibernateSpringBoot.EntityClasses.Customer;
import com.hibernateSpringBoot.EntityClasses.CustomerLedger;
import com.hibernateSpringBoot.EntityClasses.Schedule;
import com.hibernateSpringBoot.Repositories.CustomerLedgerRepository;
import com.hibernateSpringBoot.Repositories.CustomerRepository;
import com.hibernateSpringBoot.Repositories.ScheduleRepository;


@Service
public class CreditcardService {
	
	private static final Logger LOGGER = LogManager.getLogger(AppStart.class);
	@Autowired
	private CustomerRepository customerRepository;
	
	@Autowired
	private CustomerLedgerRepository customerLedgerRepository;
	
	@Autowired
	private ScheduleRepository scheduleRepository;
	
	@Autowired
	private Validation validation;
	
	BigDecimal dividor = new BigDecimal("4");

	public String addCustomer(Customer customer) {	
			if(customer.getFirstName().isEmpty() || customer.getLastName().isEmpty()|| customer.getDob().isEmpty()) {
				return "Invalid data";
			}
			customerRepository.save(customer);
			LOGGER.debug("Customer added successfully with id :: {}",customer.getcustomerId());
			return "Customer added successfully";
	}
	
	public String addLedgerWithPurchase(int custId,BigDecimal amount) {
		if(!validation.validAmount(amount)) {
			return "Provide valid amount for purchase";
		}			
		CustomerLedger ld = null;
		BigDecimal amt = amount.negate();
		BigDecimal balance= new BigDecimal("0");
		Customer c = customerRepository.findOne(custId);
		if(!validation.validCustomer(c)) {
			LOGGER.debug("Customer with given id :: {} doesn't exist",custId);
			return "Invalid Customer";
		}
		
		List<CustomerLedger> le = customerLedgerRepository.findAllBycustomerCustomerIdOrderByIdDesc(custId);
		LOGGER.debug("Initial balance is ::{}",balance);
		
		if(le.isEmpty()) {
			balance = amt;
			LOGGER.debug("Customer don't have open ledger,Updated balance is :: {}",balance);
		}else{
			balance = amt.add(le.get(0).getBalance());
			LOGGER.debug("Customer has open ledger with balance :: {},Updated balance is :: {}",le.get(0).getBalance(),balance);
		}
		
		ld = new CustomerLedger();
		ld.setType("Purchase");
		ld.setAmount(amt);
		ld.setBalance(balance);
		ld.setStatus("Open");
		ld.setCustomer(c);
		customerLedgerRepository.save(ld);
		LOGGER.info("Customer purchase is added successfully with ledgerid :: {}",ld.getId());
		addSchedule(ld,amount.divide(dividor));
		return "Customer purchase added successfully";		
	}

	public void addSchedule(CustomerLedger ledger,BigDecimal amount) {
		int cnt=1;
		while(cnt!=5) {
			Schedule schedule = new Schedule(cnt,"unpaid",amount);
			schedule.setCustomerLedger(ledger);
			scheduleRepository.save(schedule);
			cnt++;
		}
		LOGGER.debug("Schedule is added with amount :: {}",amount);
	}

	public String AddLedgerPayment(int ledgerid,BigDecimal amount) {
		CustomerLedger ledger = customerLedgerRepository.findOne(ledgerid);
		if(ledger==null) {
			return "ledger does not exists";
		}
		if(!ledger.getStatus().equals("Open")) {
			return "You don't have any open transaction for this ledger";
		}
		
		BigDecimal exactAmount = ledger.getAmount().divide(dividor).negate();
		if(exactAmount.compareTo(amount)!=0) {
			return "Amount is not equal to installment amount";
		}
		
		Customer customer = ledger.getCustomer();
		List<CustomerLedger> ledgerList = new ArrayList<>();
		ledgerList = customerLedgerRepository.findAllBycustomerCustomerIdOrderByIdDesc(customer.getcustomerId());
		CustomerLedger cusLedger = new CustomerLedger();
		cusLedger.setAmount(amount);
		LOGGER.debug("Initial balance is :: {}",ledgerList.get(0).getBalance());
		cusLedger.setBalance(amount.add(ledgerList.get(0).getBalance()));
		LOGGER.debug("Updated balance by making payment of amount ::{} is :: {}",amount,amount.add(ledgerList.get(0).getBalance()));
		cusLedger.setStatus("Completed");
		cusLedger.setType("Payment");
		cusLedger.setCustomer(customer);
		
		customerLedgerRepository.save(cusLedger);
		LOGGER.info("payment is added with Status :: Completed");
		int number = updateSchedulePayment(ledgerid);
		if(number==4) {
			ledger.setStatus("Completed");
			customerLedgerRepository.save(ledger);
			LOGGER.info("It is a last payment of given ledger::{}, Complete status of that particular ledger",ledgerid);
		}
		return "Payment completed successfully";
	}
	
	public int updateSchedulePayment(int ledgerid) {
		Schedule schedule = scheduleRepository.findTopBycustomerLedgerIdAndStatusOrderByScheduleIdAsc(ledgerid, "unpaid");
		schedule.setStatus("paid");
		scheduleRepository.save(schedule);
		LOGGER.debug("Payment status is changed to paid for given ledger :: {}",ledgerid);
		return schedule.getNumber();
	}
	
	public String fetchLatestBalance(int customerid) {
		List<CustomerLedger> ledgerList = customerLedgerRepository.findAllBycustomerCustomerIdOrderByIdDesc(customerid);
		if(ledgerList.isEmpty()) {
			return "Customer doesn't have any transaction";
		}
		String returnedValue = "";
		BigDecimal balance = ledgerList.get(0).getBalance();
//		if(balance.compareTo(new BigDecimal("0"))==1) {
//			balance = balance.negate();
//			returnedValue = "You owes " + balance + " from this customer";
//		}else {
			
//		}
		returnedValue = "Customer latest balance is " + balance;
		LOGGER.info("Customer latest balance is :: {}",balance);
		return returnedValue;
	}
	
	public List<Integer> checkOpenTransaction(int customerid) {
			List<Integer> result = new ArrayList<>();
			List<CustomerLedger> ledgerList = customerLedgerRepository.findAllBycustomerCustomerIdAndStatus(customerid, "Open");
			CustomerLedger ledger;
			Iterator itr = ledgerList.iterator();
			while(itr.hasNext()) {
				ledger = (CustomerLedger)itr.next();
				result.add(ledger.getId());
			}
			return result;
		}
}
