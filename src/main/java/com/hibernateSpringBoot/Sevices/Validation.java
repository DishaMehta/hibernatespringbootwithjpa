package com.hibernateSpringBoot.Sevices;

import java.math.BigDecimal;

import org.springframework.stereotype.Service;

import com.hibernateSpringBoot.EntityClasses.Customer;

@Service
public class Validation {
	public boolean validAmount(BigDecimal amount) {
		if(amount.compareTo(new BigDecimal("0"))==1) {
			return true;
		}
		return false;
	}
	
	public boolean validCustomer(Customer c) {
		if(c==null) {
			return false;
		}
		return true;
	}
	
}
