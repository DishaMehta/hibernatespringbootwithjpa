package com.hibernateSpringBoot.Repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.hibernateSpringBoot.EntityClasses.CustomerLedger;

@Repository
public interface CustomerLedgerRepository extends CrudRepository<CustomerLedger, Integer>{
	
	List<CustomerLedger> findAllBycustomerCustomerIdOrderByIdDesc(int customerid);
	
	List<CustomerLedger> findAllBycustomerCustomerIdAndStatus(int customerid,String status);
	
	List<CustomerLedger> findAllBycustomerCustomerIdAndStatusOrderByIdDesc(int customerid,String status);
	
	CustomerLedger findTopBycustomerCustomerIdAndStatusOrderByIdAsc(int customerid,String status);
	
}
