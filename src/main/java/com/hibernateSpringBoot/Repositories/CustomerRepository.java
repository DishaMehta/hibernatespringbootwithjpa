package com.hibernateSpringBoot.Repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.hibernateSpringBoot.EntityClasses.Customer;

@Repository
public interface CustomerRepository extends CrudRepository<Customer, Integer> {
	
}
