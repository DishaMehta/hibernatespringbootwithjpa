package com.hibernateSpringBoot.Repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.hibernateSpringBoot.EntityClasses.Schedule;

@Repository
public interface ScheduleRepository extends CrudRepository<Schedule, Integer> {

	Schedule findTopBycustomerLedgerIdAndStatusOrderByScheduleIdAsc(int ledgerid,String Status);
	
}
