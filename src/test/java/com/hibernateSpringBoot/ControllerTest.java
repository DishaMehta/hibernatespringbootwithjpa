package com.hibernateSpringBoot;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hibernateSpringBoot.EntityClasses.Customer;
import com.hibernateSpringBoot.EntityClasses.Payment;
import com.hibernateSpringBoot.EntityClasses.Purchase;

import org.springframework.http.MediaType;


@RunWith(SpringRunner.class)
@SpringBootTest(
		webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
		classes = AppStart.class
	)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application.properties")
public class ControllerTest {

	@Autowired
	private MockMvc mvc;
	
	private static final ObjectMapper om = new ObjectMapper();
	
	Customer customer = new Customer("AAA","CCC","23-08-1997");
	
//	@Test
//	public void checkOpenTransactionTest() throws Exception {
//		
//		mvc.perform( MockMvcRequestBuilders
//				.get("/checkOpenTransation/{customerid}",1)
//				.accept(MediaType.APPLICATION_JSON))
//				.andDo(print())
//				.andExpect(status().isOk())
//				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
//				.andExpect(jsonPath("$").isArray())
//				.andExpect(jsonPath("$[0]").value("2"));
//	}
	
//	@Test
//	public void fetchDetailsTest() throws Exception {
//		
//		mvc.perform( MockMvcRequestBuilders
//				.get("/customers/{customerid}",1)
//				.accept(MediaType.APPLICATION_JSON))
//				.andDo(print())
//				.andExpect(status().isOk())
//				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
//				.andExpect(content().string("Customer latest balance is -130"));		
//	}
	
//	@Test
//	public void addCustomerTest() throws Exception {
//		String jsonObject = om.writeValueAsString(customer);
//		mvc.perform( MockMvcRequestBuilders
//				.post("/customer")
//				.accept(MediaType.APPLICATION_JSON)
//				.contentType(MediaType.APPLICATION_JSON)
//				.content(jsonObject))
//				.andDo(print())
//				.andExpect(status().isOk());		
//	}
//	
//	@Test
//	public void addLedgerForPurchaseTest() throws Exception {
//		BigDecimal amount = new BigDecimal("40");
//		Purchase purchase = new Purchase(2,amount);
//		String jsonPurchase = om.writeValueAsString(purchase);
//		mvc.perform( MockMvcRequestBuilders
//				.post("/purchase")
//				.accept(MediaType.APPLICATION_JSON)
//				.contentType(MediaType.APPLICATION_JSON)
//				.content(jsonPurchase))
//				.andDo(print())
//				.andExpect(status().isOk())
//				.andExpect(content().string("Customer purchase added successfully"));
//	}
	
	@Test
	public void addLedgerForPurchaseTWithWrongAmountTest() throws Exception {
		BigDecimal amount = new BigDecimal("0");
		Purchase purchase = new Purchase(2,amount);
		String jsonPurchase = om.writeValueAsString(purchase);
		mvc.perform( MockMvcRequestBuilders
				.post("/purchase")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)
				.content(jsonPurchase))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().string("Provide valid amount for purchase"));
	}
	
	@Test
	public void addLedgerForPurchaseTWithWrongCustomerTest() throws Exception {
		BigDecimal amount = new BigDecimal("40");
		Purchase purchase = new Purchase(-6,amount);
		String jsonPurchase = om.writeValueAsString(purchase);
		mvc.perform( MockMvcRequestBuilders
				.post("/purchase")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)
				.content(jsonPurchase))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().string("Invalid Customer"));
	}
	
//	@Test
//	public void AddLedgerPaymentTest() throws Exception {
//		BigDecimal paymentAmount = new BigDecimal("10");
//		Payment payment = new Payment(8,paymentAmount);
//		String jsonPayment = om.writeValueAsString(payment);
//		mvc.perform( MockMvcRequestBuilders
//				.post("/payment")
//				.accept(MediaType.APPLICATION_JSON)
//				.contentType(MediaType.APPLICATION_JSON)
//				.content(jsonPayment))
//				.andDo(print())
//				.andExpect(status().isOk())
//				.andExpect(content().string("Payment completed successfully"));
//	}
	
//	@Test
//	public void AddLedgerPaymentWithWrongLedgerTest() throws Exception {
//		BigDecimal paymentAmount = new BigDecimal("10");
//		Payment payment = new Payment(0,paymentAmount);
//		String jsonPayment = om.writeValueAsString(payment);
//		mvc.perform( MockMvcRequestBuilders
//				.post("/payment")
//				.accept(MediaType.APPLICATION_JSON)
//				.contentType(MediaType.APPLICATION_JSON)
//				.content(jsonPayment))
//				.andDo(print())
//				.andExpect(status().isOk())
//				.andExpect(content().string("ledger does not exists"));
//	}
//	
//	@Test
//	public void AddLedgerPaymentWithWrongPaymentAmountTest() throws Exception {
//		BigDecimal paymentAmount = new BigDecimal("0");
//		Payment payment = new Payment(8,paymentAmount);
//		String jsonPayment = om.writeValueAsString(payment);
//		mvc.perform( MockMvcRequestBuilders
//				.post("/payment")
//				.accept(MediaType.APPLICATION_JSON)
//				.contentType(MediaType.APPLICATION_JSON)
//				.content(jsonPayment))
//				.andDo(print())
//				.andExpect(status().isOk())
//				.andExpect(content().string("Amount is not equal to installment amount"));
//	}
//	
//	@Test
//	public void AddLedgerPaymentWithWrongArgumentsTest() throws Exception {
//		BigDecimal paymentAmount = new BigDecimal("0");
//		Payment payment = new Payment(-97,paymentAmount);
//		String jsonPayment = om.writeValueAsString(payment);
//		mvc.perform( MockMvcRequestBuilders
//				.post("/payment")
//				.accept(MediaType.APPLICATION_JSON)
//				.contentType(MediaType.APPLICATION_JSON)
//				.content(jsonPayment))
//				.andDo(print())
//				.andExpect(status().isOk())
//				.andExpect(content().string("ledger does not exists"));
//	}
//	
//	@Test
//	public void AddLedgerPaymentWithNoOpenLedgerTest() throws Exception {
//		BigDecimal paymentAmount = new BigDecimal("10");
//		Payment payment = new Payment(2,paymentAmount);
//		String jsonPayment = om.writeValueAsString(payment);
//		mvc.perform( MockMvcRequestBuilders
//				.post("/payment")
//				.accept(MediaType.APPLICATION_JSON)
//				.contentType(MediaType.APPLICATION_JSON)
//				.content(jsonPayment))
//				.andDo(print())
//				.andExpect(status().isOk())
//				.andExpect(content().string("You don't have any open transaction for this ledger"));
//	}
}
