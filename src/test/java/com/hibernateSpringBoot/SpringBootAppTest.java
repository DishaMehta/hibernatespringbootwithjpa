package com.hibernateSpringBoot;


import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.text.DefaultEditorKit.CutAction;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import com.hibernateSpringBoot.EntityClasses.Customer;
import com.hibernateSpringBoot.EntityClasses.CustomerLedger;
import com.hibernateSpringBoot.EntityClasses.Schedule;
import com.hibernateSpringBoot.Repositories.CustomerLedgerRepository;
import com.hibernateSpringBoot.Repositories.CustomerRepository;
import com.hibernateSpringBoot.Repositories.ScheduleRepository;
import com.hibernateSpringBoot.Sevices.CreditcardService;
import com.hibernateSpringBoot.Sevices.Validation;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class SpringBootAppTest {
	
	@Mock
	private CustomerRepository customerRepo;
	
	@Mock
	private CustomerLedgerRepository customerLedgerRepo;
	
	@Mock
	private ScheduleRepository scheduleRepo;
	
	@Mock
	private Validation validation;
	
	@InjectMocks
	private CreditcardService creditcardService;
	
	@Captor
	private ArgumentCaptor<CustomerLedger> captorLedger;
	
	@Captor
	private ArgumentCaptor<Schedule> captorSchedule;
	
	@Captor
	private ArgumentCaptor<Customer> captorCustomer;
	
	Customer customer;
	
		@Before
		public void init() {
			MockitoAnnotations.initMocks(this);
			customer = new Customer("David", "Paul", "18-02-1997");
		}
		@Test
		public void AddCustomerTest() {
			when(customerRepo.save(customer)).thenReturn(customer);
			creditcardService.addCustomer(customer);
			verify(customerRepo,times(1)).save(customer);
		}
		
		@Test
		public void AddCustomerWithFirstNameTest() {
			customer = new Customer("Disha","","");
			when(customerRepo.save(customer)).thenReturn(customer);
			creditcardService.addCustomer(customer);
			verify(customerRepo,times(0)).save(customer);
		}
		
		@Test
		public void AddCustomerWithLastNameTest() {
			customer = new Customer("","Mehta","");
			when(customerRepo.save(customer)).thenReturn(customer);
			creditcardService.addCustomer(customer);
			verify(customerRepo,times(0)).save(customer);
		}
		
		@Test
		public void AddCustomerWithDobTest() {
			customer = new Customer("","","21-08-1997");
			when(customerRepo.save(customer)).thenReturn(customer);
			creditcardService.addCustomer(customer);
			verify(customerRepo,times(0)).save(customer);
		}
		
		@Test
		public void AddCustomerNoArgumentTest() {
			customer = new Customer("","","");
			when(customerRepo.save(customer)).thenReturn(customer);
			creditcardService.addCustomer(customer);
			verify(customerRepo,times(0)).save(customer);
		}
		
		@Test
		public void AddCustomerWithFirstAndLastNameTest() {
			customer = new Customer("Disha","Mehta","");
			when(customerRepo.save(customer)).thenReturn(customer);
			creditcardService.addCustomer(customer);
			verify(customerRepo,times(0)).save(customer);
		}
		
		@Test
		public void AddCustomerWithFirstNameAndDobTest() {
			customer = new Customer("Disha","","21-08-1997");
			when(customerRepo.save(customer)).thenReturn(customer);
			creditcardService.addCustomer(customer);
			verify(customerRepo,times(0)).save(customer);
		}
		
		@Test
		public void AddCustomerWithLastNameAndDobTest() {
			customer = new Customer("","Mehta","21-08-1997");
			when(customerRepo.save(customer)).thenReturn(customer);
			creditcardService.addCustomer(customer);
			verify(customerRepo,times(0)).save(customer);
		}
		
		
		//Test cases for Ledger purchase
		
		@Test
		public void AddLegerWithFirstPurchaseTest() {
			
			int customerId = 1;
			BigDecimal amount = new BigDecimal("40");
			customer = new Customer("Disha","Mehta","21-08-1997");
			CustomerLedger ledger = new CustomerLedger();
			List<CustomerLedger> ls = new ArrayList<CustomerLedger>();
			Schedule schedule = new Schedule();
			when(validation.validAmount(amount)).thenCallRealMethod();
			when(customerRepo.findOne(customerId)).thenReturn(customer);
			when(validation.validCustomer(customer)).thenCallRealMethod();
			when(customerLedgerRepo.findAllBycustomerCustomerIdOrderByIdDesc(customerId)).thenReturn(ls);
			when(customerLedgerRepo.save(ledger)).thenReturn(ledger);
			when(scheduleRepo.save(schedule)).thenReturn(schedule);
			
			assertEquals("Customer purchase added successfully",creditcardService.addLedgerWithPurchase(customerId, amount));
			verify(validation,times(1)).validAmount(amount);
			verify(customerRepo,times(1)).findOne(customerId);
			verify(validation,times(1)).validCustomer(customer);
			verify(customerLedgerRepo,times(1)).findAllBycustomerCustomerIdOrderByIdDesc(customerId);
			verify(customerLedgerRepo,times(1)).save(captorLedger.capture());
			verify(scheduleRepo,times(4)).save(captorSchedule.capture());
		}
		
		@Test
		public void AddLegerWithSecondPurchaseTest() {
			
			int customerId = 1;
			BigDecimal amount = new BigDecimal("40");
			customer = new Customer("Disha","Mehta","21-08-1997");
			BigDecimal balance = new BigDecimal("-30");
			BigDecimal ledgerAmount = new BigDecimal("80");
			CustomerLedger ledger = new CustomerLedger(customer, "Purchase",ledgerAmount,balance,"Open");
			List<CustomerLedger> ls = new ArrayList<CustomerLedger>();
			ls.add(ledger);
			Schedule schedule = new Schedule();
			when(validation.validAmount(amount)).thenCallRealMethod();
			when(customerRepo.findOne(customerId)).thenReturn(customer);
			when(validation.validCustomer(customer)).thenCallRealMethod();
			when(customerLedgerRepo.findAllBycustomerCustomerIdOrderByIdDesc(customerId)).thenReturn(ls);
			when(customerLedgerRepo.save(ledger)).thenReturn(ledger);
			when(scheduleRepo.save(schedule)).thenReturn(schedule);
			
			assertEquals("Customer purchase added successfully",creditcardService.addLedgerWithPurchase(customerId, amount));
			verify(validation,times(1)).validAmount(amount);
			verify(customerRepo,times(1)).findOne(customerId);
			verify(validation,times(1)).validCustomer(customer);
			verify(customerLedgerRepo,times(1)).findAllBycustomerCustomerIdOrderByIdDesc(customerId);
			verify(customerLedgerRepo,times(1)).save(captorLedger.capture());
			verify(scheduleRepo,times(4)).save(captorSchedule.capture());
		}
		
		@Test
		public void AddLegerWithPurchaseWithWrongAmountTest() {
			
			BigDecimal amount = new BigDecimal("-400");
			int customerId = 1;
			customer = new Customer();
			when(validation.validAmount(amount)).thenCallRealMethod();
			assertEquals("Provide valid amount for purchase",creditcardService.addLedgerWithPurchase(customerId, amount));
			verify(validation,times(1)).validAmount(amount);
			verify(customerRepo,times(0)).findOne(customerId);
			verify(validation,times(0)).validCustomer(customer);
			verify(customerLedgerRepo,times(0)).findAllBycustomerCustomerIdOrderByIdDesc(customerId);
			verify(customerLedgerRepo,times(0)).save(captorLedger.capture());
			verify(scheduleRepo,times(0)).save(captorSchedule.capture());
		}
		
		@Test
		public void AddLegerWithPurchaseWithRightAmountTest() {
			
			BigDecimal amount = new BigDecimal("40");
			int customerId = -10;
			customer = new Customer();
			when(validation.validAmount(amount)).thenCallRealMethod();
			when(customerRepo.findOne(customerId)).thenReturn(null);
			when(validation.validCustomer(customer)).thenCallRealMethod();
			assertEquals("Invalid Customer",creditcardService.addLedgerWithPurchase(customerId, amount));
			verify(validation,times(1)).validAmount(amount);
			verify(customerRepo,times(1)).findOne(customerId);
			verify(validation,times(1)).validCustomer(captorCustomer.capture());
			verify(customerLedgerRepo,times(0)).findAllBycustomerCustomerIdOrderByIdDesc(customerId);
			verify(customerLedgerRepo,times(0)).save(captorLedger.capture());
			verify(scheduleRepo,times(0)).save(captorSchedule.capture());
		}
		
		@Test
		public void AddLegerWithPurchaseWithWrongCustomerTest() {
			
			BigDecimal amount = new BigDecimal("0");
			int customerId = -10;
			customer = new Customer();
			when(validation.validAmount(amount)).thenCallRealMethod();
			assertEquals("Provide valid amount for purchase",creditcardService.addLedgerWithPurchase(customerId, amount));
			verify(validation,times(1)).validAmount(amount);
			verify(customerRepo,times(0)).findOne(customerId);
			verify(validation,times(0)).validCustomer(customer);
			verify(customerLedgerRepo,times(0)).findAllBycustomerCustomerIdOrderByIdDesc(customerId);
			verify(customerLedgerRepo,times(0)).save(captorLedger.capture());
			verify(scheduleRepo,times(0)).save(captorSchedule.capture());
			
		}
	
		@Test
		public void AddLegerWithPurchaseWithRightCustomerTest() {
			
			BigDecimal amount = new BigDecimal("0");
			int customerId = 1;
			customer = new Customer();
			when(validation.validAmount(amount)).thenCallRealMethod();
			assertEquals("Provide valid amount for purchase",creditcardService.addLedgerWithPurchase(customerId, amount));
			verify(validation,times(1)).validAmount(amount);
			verify(customerRepo,times(0)).findOne(customerId);
			verify(validation,times(0)).validCustomer(customer);
			verify(customerLedgerRepo,times(0)).findAllBycustomerCustomerIdOrderByIdDesc(customerId);
			verify(customerLedgerRepo,times(0)).save(captorLedger.capture());
			verify(scheduleRepo,times(0)).save(captorSchedule.capture());
		}
		
		@Test
		public void AddLegerWithPurchaseWithWrongArgumentsTest() {
			
			BigDecimal amount = new BigDecimal("0");
			int customerId = 0;
			customer = new Customer();
			when(validation.validAmount(amount)).thenCallRealMethod();
			assertEquals("Provide valid amount for purchase",creditcardService.addLedgerWithPurchase(customerId, amount));
			verify(validation,times(1)).validAmount(amount);
			verify(customerRepo,times(0)).findOne(customerId);
			verify(validation,times(0)).validCustomer(customer);
			verify(customerLedgerRepo,times(0)).findAllBycustomerCustomerIdOrderByIdDesc(customerId);
			verify(customerLedgerRepo,times(0)).save(captorLedger.capture());
			verify(scheduleRepo,times(0)).save(captorSchedule.capture());
		}
		
//		@Test
//		public void AddLegerWithPurchaseWithWrongAmountAndRightCustomerTest() {
//			ArgumentCaptor<CustomerLedger> captorLedger = ArgumentCaptor.forClass(CustomerLedger.class);
//			ArgumentCaptor<Schedule> captorSchedule = ArgumentCaptor.forClass(Schedule.class);
//			ArgumentCaptor<Customer> captorCustomer = ArgumentCaptor.forClass(Customer.class);
//			
//			BigDecimal amount = new BigDecimal("0");
//			int customerId = 1;
//			Customer customer = new Customer();
//			when(validation.validAmount(amount)).thenCallRealMethod();
//			assertEquals("Provide valid amount for purchase",creditcardService.addLedgerWithPurchase(customerId, amount));
//			verify(validation,times(1)).validAmount(amount);
//			verify(customerRepo,times(0)).findOne(customerId);
//			verify(validation,times(0)).validCustomer(customer);
//			verify(customerLedgerRepo,times(0)).findAllBycustomerCustomerIdOrderByIdDesc(customerId);
//			verify(customerLedgerRepo,times(0)).save(captorLedger.capture());
//			verify(scheduleRepo,times(0)).save(captorSchedule.capture());
//		}
		
		@Test
		public void AddLegerWithPurchaseWithWrongAmountAndCustomerTest() {
			
			BigDecimal amount = new BigDecimal("0");
			int customerId = -10;
			customer = new Customer();
			when(validation.validAmount(amount)).thenCallRealMethod();
			assertEquals("Provide valid amount for purchase",creditcardService.addLedgerWithPurchase(customerId, amount));
			verify(validation,times(1)).validAmount(amount);
			verify(customerRepo,times(0)).findOne(customerId);
			verify(validation,times(0)).validCustomer(customer);
			verify(customerLedgerRepo,times(0)).findAllBycustomerCustomerIdOrderByIdDesc(customerId);
			verify(customerLedgerRepo,times(0)).save(captorLedger.capture());
			verify(scheduleRepo,times(0)).save(captorSchedule.capture());
		}
		
		@Test
		public void AddLegerWithPurchaseWithNoArgumentsTest() {
			
			BigDecimal amount = new BigDecimal("0");
			int customerId = 0;
			customer = new Customer();
			when(validation.validAmount(amount)).thenCallRealMethod();
			assertEquals("Provide valid amount for purchase",creditcardService.addLedgerWithPurchase(customerId, amount));
			verify(validation,times(1)).validAmount(amount);
			verify(customerRepo,times(0)).findOne(customerId);
			verify(validation,times(0)).validCustomer(customer);
			verify(customerLedgerRepo,times(0)).findAllBycustomerCustomerIdOrderByIdDesc(customerId);
			verify(customerLedgerRepo,times(0)).save(captorLedger.capture());
			verify(scheduleRepo,times(0)).save(captorSchedule.capture());
		}
		
		//Test cases for Ledger Payment
		
		@Test
		public void AddLedgerPaymentWithPendingPaymentTest() {
			int ledgerId = 1;
			int customerId = 1;
			
			BigDecimal amount = new BigDecimal("10");
			BigDecimal mainAmount = new BigDecimal("-40");
			BigDecimal balance = new BigDecimal("-30");
			customer.setcustomerId(customerId);
			CustomerLedger ledgerPurchase = new CustomerLedger(customer, "Purchase",mainAmount,balance,"Open");
			//CustomerLedger ledgerPurchase2 = new CustomerLedger(customer, "Purchase",amount,balance,"Open");
			Schedule schedule = new Schedule(1,"unpaid",amount);
			List<CustomerLedger>listssss = new ArrayList<CustomerLedger>();
			listssss.add(ledgerPurchase);
			when(customerLedgerRepo.findOne(ledgerId)).thenReturn(ledgerPurchase);
			when(customerLedgerRepo.findAllBycustomerCustomerIdOrderByIdDesc(customerId)).thenReturn(listssss);
			when(customerLedgerRepo.save(ledgerPurchase)).thenReturn(ledgerPurchase);
			when(scheduleRepo.findTopBycustomerLedgerIdAndStatusOrderByScheduleIdAsc(ledgerId, "unpaid")).thenReturn(schedule);
			when(scheduleRepo.save(schedule)).thenReturn(schedule);
			assertEquals("Payment completed successfully",creditcardService.AddLedgerPayment(ledgerId, amount));
			verify(customerLedgerRepo,times(1)).findOne(ledgerId);
			verify(customerLedgerRepo,times(1)).findAllBycustomerCustomerIdOrderByIdDesc(customerId);
			verify(customerLedgerRepo,times(1)).save(captorLedger.capture());
			verify(scheduleRepo,times(1)).findTopBycustomerLedgerIdAndStatusOrderByScheduleIdAsc(ledgerId,"unpaid");
			verify(scheduleRepo,times(1)).save(schedule);
		}
		
		@Test
		public void AddLedgerPaymentWithPending4thPaymentTest() {
			int ledgerId = 1;
			int customerId = 1;
			
			BigDecimal amount = new BigDecimal("10");
			BigDecimal mainAmount = new BigDecimal("-40");
			BigDecimal balance = new BigDecimal("-30");
			customer.setcustomerId(customerId);
			CustomerLedger ledgerPurchase = new CustomerLedger(customer, "Purchase",mainAmount,balance,"Open");
			//CustomerLedger ledgerPurchase2 = new CustomerLedger(customer, "Purchase",amount,balance,"Open");
			Schedule schedule = new Schedule(4,"unpaid",amount);
			List<CustomerLedger>listssss = new ArrayList<CustomerLedger>();
			listssss.add(ledgerPurchase);
			when(customerLedgerRepo.findOne(ledgerId)).thenReturn(ledgerPurchase);
			when(customerLedgerRepo.findAllBycustomerCustomerIdOrderByIdDesc(customerId)).thenReturn(listssss);
			when(customerLedgerRepo.save(ledgerPurchase)).thenReturn(ledgerPurchase);
			when(scheduleRepo.findTopBycustomerLedgerIdAndStatusOrderByScheduleIdAsc(ledgerId, "unpaid")).thenReturn(schedule);
			when(scheduleRepo.save(schedule)).thenReturn(schedule);
			assertEquals("Payment completed successfully",creditcardService.AddLedgerPayment(ledgerId, amount));
			verify(customerLedgerRepo,times(1)).findOne(ledgerId);
			verify(customerLedgerRepo,times(1)).findAllBycustomerCustomerIdOrderByIdDesc(customerId);
			verify(customerLedgerRepo,times(2)).save(captorLedger.capture());
			List<CustomerLedger> ledgerList = captorLedger.getAllValues();
			assertEquals("Completed", ledgerList.get(0).getStatus());
			assertEquals("Completed", ledgerList.get(1).getStatus());
			verify(scheduleRepo,times(1)).findTopBycustomerLedgerIdAndStatusOrderByScheduleIdAsc(ledgerId,"unpaid");
			verify(scheduleRepo,times(1)).save(schedule);
		}
		
		@Test
		public void AddLedgerPaymentWithoutPendingPaymentTest() {
			int ledgerId = 1;
			int customerId = 1;
			
			BigDecimal amount = new BigDecimal("10");
			BigDecimal mainAmount = new BigDecimal("-40");
			BigDecimal balance = new BigDecimal("-30");
			customer.setcustomerId(customerId);
			CustomerLedger ledgerPurchase = new CustomerLedger(customer, "Purchase",mainAmount,balance,"Completed");
			//CustomerLedger ledgerPurchase2 = new CustomerLedger(customer, "Purchase",amount,balance,"Open");
			Schedule schedule = new Schedule(4,"unpaid",amount);
			List<CustomerLedger>listssss = new ArrayList<CustomerLedger>();
			listssss.add(ledgerPurchase);
			when(customerLedgerRepo.findOne(ledgerId)).thenReturn(ledgerPurchase);
			assertEquals("You don't have any open transaction for this ledger",creditcardService.AddLedgerPayment(ledgerId, amount));
			verify(customerLedgerRepo,times(1)).findOne(ledgerId);
			verify(customerLedgerRepo,never()).findAllBycustomerCustomerIdOrderByIdDesc(customerId);
			verify(customerLedgerRepo,never()).save(captorLedger.capture());
			verify(scheduleRepo,never()).findTopBycustomerLedgerIdAndStatusOrderByScheduleIdAsc(ledgerId,"unpaid");
			verify(scheduleRepo,never()).save(schedule);
		}
		
		@Test
		public void AddLedgerPaymentWithWrongLedgerTest() {
			int ledgerId = -1;
			int customerId = 1;
			
			BigDecimal amount = new BigDecimal("10");
			customer.setcustomerId(customerId);
			CustomerLedger ledgerPurchase = new CustomerLedger();
			Schedule schedule = new Schedule(4,"unpaid",amount);
			when(customerLedgerRepo.findOne(ledgerId)).thenReturn(null);
			assertEquals("ledger does not exists",creditcardService.AddLedgerPayment(ledgerId, amount));
			verify(customerLedgerRepo,times(1)).findOne(ledgerId);
			verify(customerLedgerRepo,never()).findAllBycustomerCustomerIdOrderByIdDesc(customerId);
			verify(customerLedgerRepo,never()).save(captorLedger.capture());
			verify(scheduleRepo,never()).findTopBycustomerLedgerIdAndStatusOrderByScheduleIdAsc(ledgerId,"unpaid");
			verify(scheduleRepo,never()).save(schedule);
		}
		
		
		@Test
		public void AddLedgerPaymentWithWrongAmountTest() {
			int ledgerId = 1;
			int customerId = 1;
			
			BigDecimal amount = new BigDecimal("-23");
			BigDecimal mainAmount = new BigDecimal("-40");
			BigDecimal balance = new BigDecimal("-30");
			customer.setcustomerId(customerId);
			CustomerLedger ledgerPurchase = new CustomerLedger(customer, "Purchase",mainAmount,balance,"Open");
			Schedule schedule = new Schedule(1,"unpaid",amount);
			when(customerLedgerRepo.findOne(ledgerId)).thenReturn(ledgerPurchase);
			assertEquals("Amount is not equal to installment amount",creditcardService.AddLedgerPayment(ledgerId, amount));
			verify(customerLedgerRepo,times(1)).findOne(ledgerId);
			verify(customerLedgerRepo,never()).findAllBycustomerCustomerIdOrderByIdDesc(customerId);
			verify(customerLedgerRepo,never()).save(captorLedger.capture());
			verify(scheduleRepo,never()).findTopBycustomerLedgerIdAndStatusOrderByScheduleIdAsc(ledgerId,"unpaid");
			verify(scheduleRepo,never()).save(schedule);
		}
		
		@Test
		public void AddLedgerPaymentWithWrongArgumentsTest() {
			int ledgerId = -1;
			int customerId = 1;
			
			BigDecimal amount = new BigDecimal("-23");
			customer.setcustomerId(customerId);
			CustomerLedger ledgerPurchase = new CustomerLedger();
			//CustomerLedger ledgerPurchase2 = new CustomerLedger(customer, "Purchase",amount,balance,"Open");
			Schedule schedule = new Schedule(4,"unpaid",amount);
			when(customerLedgerRepo.findOne(ledgerId)).thenReturn(null);
			assertEquals("ledger does not exists",creditcardService.AddLedgerPayment(ledgerId, amount));
			verify(customerLedgerRepo,times(1)).findOne(ledgerId);
			verify(customerLedgerRepo,never()).findAllBycustomerCustomerIdOrderByIdDesc(customerId);
			verify(customerLedgerRepo,never()).save(captorLedger.capture());
			verify(scheduleRepo,never()).findTopBycustomerLedgerIdAndStatusOrderByScheduleIdAsc(ledgerId,"unpaid");
			verify(scheduleRepo,never()).save(schedule);
		}
		
		@Test
		public void AddLedgerPaymentWithOnlyAmountTest() {
			int ledgerId = 0;
			int customerId = 1;
			
			BigDecimal amount = new BigDecimal("10");
			customer.setcustomerId(customerId);
			CustomerLedger ledgerPurchase = new CustomerLedger();
			//CustomerLedger ledgerPurchase2 = new CustomerLedger(customer, "Purchase",amount,balance,"Open");
			Schedule schedule = new Schedule(4,"unpaid",amount);
			when(customerLedgerRepo.findOne(ledgerId)).thenReturn(null);
			assertEquals("ledger does not exists",creditcardService.AddLedgerPayment(ledgerId, amount));
			verify(customerLedgerRepo,times(1)).findOne(ledgerId);
			verify(customerLedgerRepo,never()).findAllBycustomerCustomerIdOrderByIdDesc(customerId);
			verify(customerLedgerRepo,never()).save(captorLedger.capture());
			verify(scheduleRepo,never()).findTopBycustomerLedgerIdAndStatusOrderByScheduleIdAsc(ledgerId,"unpaid");
			verify(scheduleRepo,never()).save(schedule);
		}
		
		@Test
		public void AddLedgerPaymentWithOnlyLedgerTest() {
			int ledgerId = 1;
			int customerId = 1;
			
			BigDecimal amount = new BigDecimal("0");
			BigDecimal mainAmount = new BigDecimal("-40");
			BigDecimal balance = new BigDecimal("-30");
			customer.setcustomerId(customerId);
			CustomerLedger ledgerPurchase = new CustomerLedger(customer, "Purchase",mainAmount,balance,"Open");
			//CustomerLedger ledgerPurchase2 = new CustomerLedger(customer, "Purchase",amount,balance,"Open");
			Schedule schedule = new Schedule(4,"unpaid",amount);
			when(customerLedgerRepo.findOne(ledgerId)).thenReturn(ledgerPurchase);
			assertEquals("Amount is not equal to installment amount",creditcardService.AddLedgerPayment(ledgerId, amount));
			verify(customerLedgerRepo,times(1)).findOne(ledgerId);
			verify(customerLedgerRepo,never()).findAllBycustomerCustomerIdOrderByIdDesc(customerId);
			verify(customerLedgerRepo,never()).save(captorLedger.capture());
			verify(scheduleRepo,never()).findTopBycustomerLedgerIdAndStatusOrderByScheduleIdAsc(ledgerId,"unpaid");
			verify(scheduleRepo,never()).save(schedule);
		}
		
		@Test
		public void AddLedgerPaymentWithOnlyWrongAmountTest() {
			int ledgerId = 0;
			int customerId = 1;
			
			BigDecimal amount = new BigDecimal("-22");
			customer.setcustomerId(customerId);
			CustomerLedger ledgerPurchase = new CustomerLedger();
			//CustomerLedger ledgerPurchase2 = new CustomerLedger(customer, "Purchase",amount,balance,"Open");
			Schedule schedule = new Schedule(4,"unpaid",amount);
			when(customerLedgerRepo.findOne(ledgerId)).thenReturn(null);
			assertEquals("ledger does not exists",creditcardService.AddLedgerPayment(ledgerId, amount));
			verify(customerLedgerRepo,times(1)).findOne(ledgerId);
			verify(customerLedgerRepo,never()).findAllBycustomerCustomerIdOrderByIdDesc(customerId);
			verify(customerLedgerRepo,never()).save(captorLedger.capture());
			verify(scheduleRepo,never()).findTopBycustomerLedgerIdAndStatusOrderByScheduleIdAsc(ledgerId,"unpaid");
			verify(scheduleRepo,never()).save(schedule);
		}
		
		@Test
		public void AddLedgerPaymentWithOnlyWrongLedgerTest() {
			int ledgerId = -1;
			int customerId = 1;
			
			BigDecimal amount = new BigDecimal("0");
			customer.setcustomerId(customerId);
			CustomerLedger ledgerPurchase = new CustomerLedger();
			//CustomerLedger ledgerPurchase2 = new CustomerLedger(customer, "Purchase",amount,balance,"Open");
			Schedule schedule = new Schedule(4,"unpaid",amount);
			when(customerLedgerRepo.findOne(ledgerId)).thenReturn(null);
			assertEquals("ledger does not exists",creditcardService.AddLedgerPayment(ledgerId, amount));
			verify(customerLedgerRepo,times(1)).findOne(ledgerId);
			verify(customerLedgerRepo,never()).findAllBycustomerCustomerIdOrderByIdDesc(customerId);
			verify(customerLedgerRepo,never()).save(captorLedger.capture());
			verify(scheduleRepo,never()).findTopBycustomerLedgerIdAndStatusOrderByScheduleIdAsc(ledgerId,"unpaid");
			verify(scheduleRepo,never()).save(schedule);
		}
		
		//Test cases for fetching latest balance
		
		@Test
		public void fetchLatestBalanceForRightCustometTest() {
			int customerId = 1;
			BigDecimal ledgerAmount= new BigDecimal("10");
			BigDecimal balance = new BigDecimal("-30");
			CustomerLedger ledger = new CustomerLedger(customer, "Purchase",ledgerAmount,balance,"Open");
			List<CustomerLedger> ledgerList = new ArrayList<CustomerLedger>();
			ledgerList.add(ledger);
			when(customerLedgerRepo.findAllBycustomerCustomerIdOrderByIdDesc(customerId)).thenReturn(ledgerList);
			assertEquals("Customer latest balance is " + balance,creditcardService.fetchLatestBalance(customerId));
			verify(customerLedgerRepo,times(1)).findAllBycustomerCustomerIdOrderByIdDesc(customerId);
		}
		
		@Test
		public void fetchLatestBalanceForWrongCustometTest() {
			int customerId = -1;
			BigDecimal ledgerAmount= new BigDecimal("10");
			BigDecimal balance = new BigDecimal("-30");
			CustomerLedger ledger = new CustomerLedger(customer, "Purchase",ledgerAmount,balance,"Open");
			List<CustomerLedger> ledgerList = new ArrayList<CustomerLedger>();
			when(customerLedgerRepo.findAllBycustomerCustomerIdOrderByIdDesc(customerId)).thenReturn(ledgerList);
			assertEquals("Customer doesn't have any transaction",creditcardService.fetchLatestBalance(customerId));
			verify(customerLedgerRepo,times(1)).findAllBycustomerCustomerIdOrderByIdDesc(customerId);
		}
		
		//Test cases for checking Open transaction
		
		@Test
		public void checkOpenTransactionWithRightCustomerTest() {
			int customerId =1;
			customer = new Customer("David", "Paul", "18-02-1997");
			List<CustomerLedger> ledgerList = new ArrayList<CustomerLedger>();
			BigDecimal ledgerAmount= new BigDecimal("10");
			BigDecimal balance = new BigDecimal("-30");
			CustomerLedger ledger = new CustomerLedger(customer, "Purchase",ledgerAmount,balance,"Open");
			ledger.setId(1);
			ledgerList.add(ledger);
			when(customerLedgerRepo.findAllBycustomerCustomerIdAndStatus(customerId, "Open")).thenReturn(ledgerList);
			List<Integer> expected = new ArrayList<Integer>();
			expected.add(1);
			assertEquals(expected,creditcardService.checkOpenTransaction(customerId));
			verify(customerLedgerRepo,times(1)).findAllBycustomerCustomerIdAndStatus(customerId,"Open");

		}
		
		@Test
		public void checkOpenTransactionWithWrongCustomerTest() {
			int customerId =-10;
			List<CustomerLedger> ledgerList = new ArrayList<CustomerLedger>();
			when(customerLedgerRepo.findAllBycustomerCustomerIdAndStatus(customerId, "Open")).thenReturn(ledgerList);
			List<Integer> expected = new ArrayList<Integer>();
			assertEquals(expected,creditcardService.checkOpenTransaction(customerId));
			verify(customerLedgerRepo,times(1)).findAllBycustomerCustomerIdAndStatus(customerId,"Open");

		}
}
